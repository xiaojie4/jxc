package com.atguigu.jxc.result.vo.damageList;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DamageRowsDTO {
    @JsonProperty("damageListGoodsId")
    private Integer damageListGoodsId;
    @JsonProperty("goodsId")
    private Integer goodsId;
    @JsonProperty("goodsCode")
    private String goodsCode;
    @JsonProperty("goodsName")
    private String goodsName;
    @JsonProperty("goodsModel")
    private String goodsModel;
    @JsonProperty("goodsUnit")
    private String goodsUnit;
    @JsonProperty("goodsNum")
    private Integer goodsNum;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("damageListId")
    private Integer damageListId;
    @JsonProperty("goodsTypeId")
    private Integer goodsTypeId;
}