package com.atguigu.jxc.result.vo.unit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class UnitVo {
    @JsonProperty("rows")
    private List<UnitRowsDTO> rows;
}
