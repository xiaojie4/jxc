package com.atguigu.jxc.result.vo.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class CustomerVo {


    @JsonProperty("total")
    private Integer total;
    @JsonProperty("rows")
    private List<CustomerRowsDTO> rows;


}
