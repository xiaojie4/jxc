package com.atguigu.jxc.result.vo.goods;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class GoodsVo {

    @JsonProperty("total")
    private Integer total;
    @JsonProperty("rows")
    private List<GoodsRowsDTO> rows;


}
