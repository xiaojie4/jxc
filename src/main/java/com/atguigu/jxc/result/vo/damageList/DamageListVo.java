package com.atguigu.jxc.result.vo.damageList;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import java.util.List;

@Data
public class DamageListVo {
    @JsonProperty("rows")
    private List<DamageRowsDTO> rows;
}
