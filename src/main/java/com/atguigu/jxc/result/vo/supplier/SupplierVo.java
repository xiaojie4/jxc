package com.atguigu.jxc.result.vo.supplier;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


import java.util.List;

// 供应商对象模型
@Data
public class SupplierVo {
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("rows")
    private List<SupplierRowsDTO> rows;
}
