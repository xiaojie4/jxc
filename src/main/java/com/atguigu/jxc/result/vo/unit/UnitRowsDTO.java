package com.atguigu.jxc.result.vo.unit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UnitRowsDTO {
    @JsonProperty("unitId")
    private Integer unitId;
    @JsonProperty("unitName")
    private String unitName;
}