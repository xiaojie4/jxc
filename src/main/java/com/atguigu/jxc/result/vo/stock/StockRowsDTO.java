package com.atguigu.jxc.result.vo.stock;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StockRowsDTO {
    @JsonProperty("goodsId")
    private Integer goodsId;
    @JsonProperty("goodsCode")
    private String goodsCode;
    @JsonProperty("goodsName")
    private String goodsName;
    @JsonProperty("inventoryQuantity")
    private Integer inventoryQuantity;
    @JsonProperty("lastPurchasingPrice")
    private Double lastPurchasingPrice;
    @JsonProperty("minNum")
    private Integer minNum;
    @JsonProperty("goodsModel")
    private String goodsModel;
    @JsonProperty("goodsProducer")
    private String goodsProducer;
    @JsonProperty("purchasingPrice")
    private Double purchasingPrice;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("sellingPrice")
    private Double sellingPrice;
    @JsonProperty("state")
    private Integer state;
    @JsonProperty("goodsUnit")
    private String goodsUnit;
    @JsonProperty("goodsTypeId")
    private Integer goodsTypeId;
    @JsonProperty("goodsTypeName")
    private String goodsTypeName;
    @JsonProperty("saleTotal")
    private Integer saleTotal;
}