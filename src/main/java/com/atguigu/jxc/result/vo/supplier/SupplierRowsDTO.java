package com.atguigu.jxc.result.vo.supplier;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SupplierRowsDTO {
    @JsonProperty("supplierId")
    private Integer supplierId;
    @JsonProperty("supplierName")
    private String supplierName;
    @JsonProperty("contacts")
    private String contacts;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("address")
    private String address;
    @JsonProperty("remarks")
    private String remarks;
}