package com.atguigu.jxc.result.vo.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CustomerRowsDTO {
    @JsonProperty("customerId")
    private Integer customerId;
    @JsonProperty("customerName")
    private String customerName;
    @JsonProperty("contacts")
    private String contacts;
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("address")
    private String address;
    @JsonProperty("remarks")
    private String remarks;
}