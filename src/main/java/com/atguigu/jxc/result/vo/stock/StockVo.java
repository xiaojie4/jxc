package com.atguigu.jxc.result.vo.stock;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class StockVo {


    @JsonProperty("total")
    private Integer total;
    @JsonProperty("rows")
    private List<StockRowsDTO> rows;



}
