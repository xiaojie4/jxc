package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ResponseVo;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowService overflowService;

    //    新增报溢单
    @PostMapping("/save")
    public ResponseVo save(HttpSession httpSession, OverflowList overflowList,
                           @RequestParam("overflowListGoodsStr") String overflowListGoodsStr) {
        boolean result = overflowService.save(httpSession, overflowList, overflowListGoodsStr);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(101, "请求失败", "请求失败");
        }
    }


    //    报溢单查询
    @PostMapping("/list")
    public Map<String, List> getList(@RequestParam("sTime") String sTime,
                                      @RequestParam("eTime") String eTime) throws ParseException {
        Map<String, List> stringListHashMap = new HashMap<>();

        List<OverflowList> overflowLists = overflowService.getList(sTime, eTime);

        stringListHashMap.put("rows",overflowLists);
        return stringListHashMap;
    }

    //    报溢单商品信息
    @PostMapping("/goodsList")
    public Map goodsList(@RequestParam("overflowListId") Integer overflowListId) {
        HashMap<String, List> stringListHashMap = new HashMap<>();

        List<OverflowListGoods> goodsList = overflowService.getGoodsList(overflowListId);
        stringListHashMap.put("rows",goodsList);
        return stringListHashMap;

    }
}
