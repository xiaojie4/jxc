package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ResponseVo;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.result.vo.supplier.SupplierVo;
import com.atguigu.jxc.service.TSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//供货商
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private TSupplierService tSupplierService;

    //   分页查询供应商
    @PostMapping("/list")
    public SupplierVo supplierList(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows,
                                   @RequestParam("supplierName") String supplierName) {

        SupplierVo supplierVo = tSupplierService.getSupplierList(page, rows, supplierName);
        return supplierVo;
    }

    //      供应商添加或修改
    @PostMapping("/save")
    public ResponseVo saveOrChangeSupplier(@RequestParam(value = "supplierId", required = false) Long supplierId,
                                           Supplier supplier) {
        boolean result = tSupplierService.saveOrChangeSupplier(supplierId, supplier);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(3001, "请求失败", null);
        }
    }

    //    删除供应商(支持批量删除)
    @PostMapping("/delete")
    public ResponseVo deleteSupplier(@RequestParam("ids") String ids) {
        boolean result = tSupplierService.deleteSupplier(ids);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(3001, "请求失败", null);
        }
    }
}
