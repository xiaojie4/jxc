package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ResponseVo;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.result.vo.damageList.DamageListVo;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//商品报损
@RequestMapping("/damageListGoods")
@RestController
public class DamageController {

    @Autowired
    private DamageService damageService;

    //保存报损单
    @PostMapping("/save")
    public ResponseVo save(HttpSession session,
                           DamageList damageList,
                           @RequestParam("damageListGoodsStr") String damageListGoodsStr) {

        boolean result = damageService.save(session, damageList, damageListGoodsStr);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(101, "请求失败", "请求失败");
        }
    }

    //    报损单查询
    @PostMapping("/list")
    public Map getList(@RequestParam("sTime") String sTime,
                       @RequestParam("eTime") String eTime) throws ParseException {
        HashMap<String, List> stringListHashMap = new HashMap<>();

        List<DamageList> damageLists = damageService.list(sTime, eTime);
        stringListHashMap.put("rows",damageLists);

        return stringListHashMap;
    }


    //    查询报损单商品信息
    @PostMapping("/goodsList")
    public DamageListVo getDamageList(@RequestParam(value = "damageListId", required = false) Integer damageListId) {
        DamageListVo damageListVo = damageService.getDamageList(damageListId);
        return damageListVo;
    }

}
