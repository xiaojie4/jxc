package com.atguigu.jxc.controller;

import com.atguigu.jxc.anno.Log;
import com.atguigu.jxc.domain.ResponseVo;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.result.vo.goods.GoodsVo;
import com.atguigu.jxc.result.vo.stock.StockVo;
import com.atguigu.jxc.result.vo.unit.UnitVo;
import com.atguigu.jxc.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @description 商品信息Controller
 */
@Slf4j
@RestController
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Log
    @PostMapping("/goods/listInventory")
    public StockVo listInventory(@RequestParam(value = "page") Integer page,
                                 @RequestParam(value = "rows") Integer rows,
                                 @RequestParam(value = "codeOrName", required = false) String codeOrName,
                                 @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {
        StockVo stockVo = goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
        return stockVo;
    }

    @Log
    //    查询所有商品单位
    @PostMapping("/unit/list")
    public UnitVo getUnitList() {
        UnitVo unitList = goodsService.getUnitList();
        return unitList;
    }


    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Log

    @PostMapping("/goods/list")
    public GoodsVo getGoodsList(@RequestParam("page") Integer page,
                                @RequestParam("rows") Integer rows,
                                @RequestParam(value = "goodsName", required = false) String goodsName,
                                @RequestParam(value = "goodsTypeId", required = false) Integer goodsTypeId) {
        GoodsVo goodsVo = goodsService.getGoodsList(page, rows, goodsName, goodsTypeId);
        return goodsVo;
    }
    @Log
    //    新增分类
    @PostMapping("/goodsType/save")
    public ResponseVo addGoodType(@RequestParam("goodsTypeName") String goodsTypeName,
                                  @RequestParam("pId") Integer pId) {
        boolean result = goodsService.addGoodType(goodsTypeName, pId);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(100, "请求失败", null);
        }
    }
    @Log
    //    删除分类
    @PostMapping("/goodsType/delete")
    public ResponseVo deleteGoodType(@RequestParam("goodsTypeId") Integer goodsTypeId) {

        boolean result = goodsService.deleteGoodType(goodsTypeId);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(100, "请求失败", null);
        }
    }

    @Log
    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/goods/save")
    public ResponseVo saveOrChangeGoods(@RequestParam(value = "goodsId", required = false) Long goodsId,
                                        Goods goods) {
        boolean result = goodsService.saveOrChangeGoods(goodsId, goods);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(100, "请求失败", null);
        }
    }


    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/delete")
    public ResponseVo deleteGoods(@RequestParam("goodsId") Integer goodsId) {
        boolean result = goodsService.deleteGoods(goodsId);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(101, "删除失败", "删除失败");
        }
    }


    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getNoInventoryQuantity")
    public GoodsVo getNoInventoryQuantity(@RequestParam("page") Integer page,
                                          @RequestParam("rows") Integer rows,
                                          @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        GoodsVo goodsVo = goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
        return goodsVo;
    }


    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getHasInventoryQuantity")
    public GoodsVo getHasInventoryQuantity(@RequestParam("page") Integer page,
                                           @RequestParam("rows") Integer rows,
                                           @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        GoodsVo goodsVo = goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
        return goodsVo;
    }


    /**
     * 添加商品期初库存或者修改成本价
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping("/goods/saveStock")
    public ResponseVo addOrChangeStock(@RequestParam("goodsId") Integer goodsId,
                                        @RequestParam("inventoryQuantity") Integer inventoryQuantity,
                                        @RequestParam("purchasingPrice") double purchasingPrice) {
      boolean result=  goodsService.addOrChangeStock(goodsId,inventoryQuantity,purchasingPrice);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(101, "请求失败", "请求失败");
        }
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/deleteStock")
    public ResponseVo deleteStock(@RequestParam("goodsId")Integer goodsId){
     boolean result=   goodsService.deleteStock(goodsId);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(101, "请求失败", "请求失败");
        }
    }


    /**
     * 查询库存报警商品信息
     * @return
     */

}
