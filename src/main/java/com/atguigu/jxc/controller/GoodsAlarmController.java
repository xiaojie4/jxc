package com.atguigu.jxc.controller;


import com.atguigu.jxc.dao.GoodsAlarmDao;
import com.atguigu.jxc.result.vo.goods.GoodsRowsDTO;
import com.atguigu.jxc.service.GoodsAlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/goods")
public class GoodsAlarmController {

    @Autowired
    private GoodsAlarmService goodsAlarmService;

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public List<GoodsRowsDTO> listAlarm() {
        List<GoodsRowsDTO> list = goodsAlarmService.listAlarm();
        return list;
    }

}
