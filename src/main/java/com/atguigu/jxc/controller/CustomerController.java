package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ResponseVo;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.result.vo.customer.CustomerVo;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;


//客户管理
@RequestMapping("/customer")
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;


    //    客户列表分页(名称模糊查询)
    @PostMapping("/list")
    public CustomerVo getCustomerList(@RequestParam(value = "page") Integer page,
                                      @RequestParam(value = "rows") Integer rows,
                                      @RequestParam(value = "customerName", required = false) String customerName) {

        CustomerVo customerVo = customerService.getCustomerList(page, rows, customerName);
        return customerVo;
    }

    //    客户添加或修改
    @PostMapping("/save")
    public ResponseVo saveOrChangeCustomer(@RequestParam(value = "customerId",required = false) Long customerId,
                                           Customer customer) {
        boolean result = customerService.saveOrChangeCustomer(customerId, customer);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(100, "请求失败", null);
        }
    }


//   客户删除（支持批量删除）
    @PostMapping("/delete")
    public ResponseVo deleteCustomer(@RequestParam("ids") String ids){
      boolean result=  customerService.batchDelete(ids);
        if (result) {
            return ResponseVo.build(100, "请求成功", null);
        } else {
            return ResponseVo.build(100, "请求失败", "删除失败");
        }
    }
}
