package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.result.vo.goods.GoodsRowsDTO;
import com.atguigu.jxc.result.vo.goods.GoodsVo;
import com.atguigu.jxc.result.vo.stock.StockRowsDTO;
import com.atguigu.jxc.result.vo.stock.StockVo;
import com.atguigu.jxc.result.vo.unit.UnitRowsDTO;
import com.atguigu.jxc.result.vo.unit.UnitVo;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public StockVo listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
//        查询所有商品
        List<StockRowsDTO> stockRows = goodsDao.listInventory(page - 1, rows, codeOrName, goodsTypeId);
//        所有商品总数量
        int total = goodsDao.getTotalGoods();

//      商品类型
        List<StockRowsDTO> collect = stockRows.stream()
                .map(item -> {
//                    查询商品类型
                    String goodType = goodsDao.getGoodTypeName(item.getGoodsTypeId());
//                    查询商品销售数量
                    Integer saleTotal = goodsDao.getGoodSaleTotal(item.getGoodsId());

                    StockRowsDTO stockRowsDTO = new StockRowsDTO();
                    stockRowsDTO.setGoodsId(item.getGoodsId());
                    stockRowsDTO.setGoodsCode(item.getGoodsCode());
                    stockRowsDTO.setGoodsName(item.getGoodsName());
                    stockRowsDTO.setInventoryQuantity(item.getInventoryQuantity());
                    stockRowsDTO.setLastPurchasingPrice(item.getLastPurchasingPrice());
                    stockRowsDTO.setMinNum(item.getMinNum());
                    stockRowsDTO.setGoodsModel(item.getGoodsModel());
                    stockRowsDTO.setGoodsProducer(item.getGoodsProducer());
                    stockRowsDTO.setPurchasingPrice(item.getPurchasingPrice());
                    stockRowsDTO.setRemarks(item.getRemarks());
                    stockRowsDTO.setSellingPrice(item.getSellingPrice());
                    stockRowsDTO.setState(item.getState());
                    stockRowsDTO.setGoodsUnit(item.getGoodsUnit());
                    stockRowsDTO.setGoodsTypeId(item.getGoodsTypeId());
                    stockRowsDTO.setGoodsTypeName(goodType);
                    stockRowsDTO.setSaleTotal(saleTotal);

                    return stockRowsDTO;
                }).collect(Collectors.toList());

        StockVo stockVo = new StockVo();
        stockVo.setTotal(total);

        stockVo.setRows(collect);

        return stockVo;
    }

    @Override
    public UnitVo getUnitList() {
        List<UnitRowsDTO> unitList = goodsDao.getUnitList();
        UnitVo unitVo = new UnitVo();
        unitVo.setRows(unitList);
        return unitVo;
    }

    @Override
    public GoodsVo getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        if (goodsTypeId == 1) {
//            查所有
            List<GoodsRowsDTO> goodsRowsDTO = goodsDao.getGoodsALList(page - 1, rows, goodsName);
//        查询商品总记录数
            Integer total = goodsDao.totalALLGoods();
            GoodsVo goodsVo = new GoodsVo();
            goodsVo.setRows(goodsRowsDTO);
            goodsVo.setTotal(total);
            return goodsVo;
        } else {

//        查询商品详情列表
            List<GoodsRowsDTO> goodsRowsDTO = goodsDao.getGoodsList(page - 1, rows, goodsName, goodsTypeId);
//        查询商品总记录数
            Integer total = goodsDao.totalGoods(goodsTypeId);
            GoodsVo goodsVo = new GoodsVo();
            goodsVo.setRows(goodsRowsDTO);
            goodsVo.setTotal(total);
            return goodsVo;
        }
    }

    @Override
    public boolean addGoodType(String goodsTypeName, Integer pId) {
        int goodsTypeStatus;
        if (pId == 1) {
//            说明是所有分类下的分类,暂时没有子节点
            goodsTypeStatus = 0;
            boolean result = goodsDao.addGoodType(goodsTypeName, pId, goodsTypeStatus);
            return result;
        } else {
//            有子菜单,需要修改pid所对应记录的goodTypeStatus状态为1
            goodsTypeStatus = 1;
            boolean result = false;
//            新增菜单,默认都没有子节点
            result = goodsDao.addGoodType(goodsTypeName, pId, 0);
//            查询有没有类别的pid是当前传入的pid
            List<GoodsType> goodsType = goodsDao.getGoodTypeByPid(pId);
            for (GoodsType type : goodsType) {
//                修改父节点的goodTypeStatus
                result = goodsDao.updateGoodsTypeStatus(type.getPId(), goodsTypeStatus);
            }
            return result;
        }
    }

    @Override
    public boolean deleteGoodType(Integer goodsTypeId) {
//        先根据goodsTypeId查询到当前整行记录,保存pid
        GoodsType goodsType = goodsDao.getGoodTypeById(goodsTypeId);
//        记录pid,待会判断父节点,是否要更新goodsTypeStatus
        Integer pId = goodsType.getPId();

//        删除当前选中节点
        boolean result = goodsDao.deleteGoodType(goodsTypeId);
        if (result) {
            List<GoodsType> goodTypeByPid = goodsDao.getGoodTypeByPid(pId);
            if (goodTypeByPid == null || goodTypeByPid.size() == 0) {
//          如果父节点没有子节点了,修改其goodsTypeStatus的值
                goodsDao.updateGoodsTypeStatus(pId, 0);
            }
        }
        return result;
    }

    @Override
    public boolean saveOrChangeGoods(Long goodsId, Goods goods) {
//        根据goodsId判断是添加还是修改
        if (goodsId != null) {
//            说明是修改
            goods.setInventoryQuantity(0); //默认库存0
            goods.setState(0);//0表示初始值,已入库
            boolean result = goodsDao.updateGoods(goodsId, goods);
            return result;
        }
//        这里说明是新增商品
        goods.setInventoryQuantity(0); //默认库存0
        goods.setState(0);//0表示初始值,已入库
        boolean result = goodsDao.saveGoods(goods);
        return result;
    }

    @Override
    public boolean deleteGoods(Integer goodsId) {
//        先查询到要删除的商品
        Goods goods = goodsDao.getGoodsByGoodId(goodsId);
//        判断当前商品状态是否能删除(只有当状态=0才能删除)
        if (goods.getState() == 0) {
//            可以删除
            boolean result = goodsDao.deleteGoods(goodsId);
            return result;
        }
        return false;
    }

    @Override
    public GoodsVo getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
//        查询无库存的商品
        List<GoodsRowsDTO> goodsRowsDTO = goodsDao.getNoInventoryQuantity(page - 1, rows, nameOrCode);
//        统计无库存商品记录数
        Integer total = goodsDao.totalNoInventoryQuantity();

        GoodsVo goodsVo = new GoodsVo();
        goodsVo.setTotal(total);
        goodsVo.setRows(goodsRowsDTO);
        return goodsVo;
    }

    @Override
    public GoodsVo getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
//        查询有库存商品
        List<GoodsRowsDTO> goodsRowsDTO = goodsDao.getHasInventoryQuantity(page - 1, rows, nameOrCode);

//        统计有库存商品
        Integer total = goodsDao.totalHasInventoryQuantity();
        GoodsVo goodsVo = new GoodsVo();
        goodsVo.setTotal(total);
        goodsVo.setRows(goodsRowsDTO);
        return goodsVo;
    }

    @Override
    public boolean addOrChangeStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        boolean result = goodsDao.addOrChangeStock(goodsId, inventoryQuantity, purchasingPrice);
        return result;
    }

    @Override
    public boolean deleteStock(Integer goodsId) {
        boolean result = goodsDao.deleteStock(goodsId);
        return result;
    }
}
