package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsAlarmDao;
import com.atguigu.jxc.result.vo.goods.GoodsRowsDTO;
import com.atguigu.jxc.service.GoodsAlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsAlarmServiceImpl implements GoodsAlarmService {
    @Autowired
    private GoodsAlarmDao goodsAlarmDao;

    @Override
    public List<GoodsRowsDTO> listAlarm() {
        List<GoodsRowsDTO> list = goodsAlarmDao.listAlarm();

        return list;
    }
}
