package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.result.vo.customer.CustomerRowsDTO;
import com.atguigu.jxc.result.vo.customer.CustomerVo;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public CustomerVo getCustomerList(Integer page, Integer rows, String customerName) {
//        查询客户列表
        List<CustomerRowsDTO> customerList = customerDao.getCustomerList(page - 1, rows, customerName);
//        查询客户总数量
        Integer totalCustomer = customerDao.getTotalCustomer();

        CustomerVo customerVo = new CustomerVo();
        customerVo.setTotal(totalCustomer);
        customerVo.setRows(customerList);

        return customerVo;
    }

    @Override
    public boolean saveOrChangeCustomer(Long customerId, Customer customer) {
//        判断是要新增还是修改
        if (customerId != null) {
//            说明是修改
            boolean result = customerDao.updateCustomer(customerId, customer);
            return result;
        }
//        说明是新增
       boolean result= customerDao.save(customer);
        return result;
    }

    @Override
    public boolean batchDelete(String ids) {
        String[] split = ids.split(",");
        List<String> list = Arrays.asList(split);
       boolean result= customerDao.deleteCustomer(list);
        return result;
    }
}
