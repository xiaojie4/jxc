package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowService;
import com.atguigu.jxc.util.DateUtil;
import com.atguigu.jxc.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Service
public class OverflowListServiceImpl implements OverflowService {
    @Autowired
    private OverflowDao overflowDao;

    @Override
    public boolean save(HttpSession httpSession, OverflowList overflowList, String overflowListGoodsStr) {
        User user = (User) httpSession.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        OverflowListGoods[] overflowListGood = JsonUtils.fromToObject(overflowListGoodsStr, OverflowListGoods[].class);
        List<OverflowListGoods> overflowListGoods = Arrays.asList(overflowListGood);
        boolean result = overflowDao.saveOverflowList(overflowList);
        if (result) {
            //        取出回填主键
            Integer overflowListId = overflowList.getOverflowListId();
            for (OverflowListGoods listGood : overflowListGoods) {
                listGood.setOverflowListId(overflowListId);
                overflowDao.saveOverflowListGoods(listGood);
            }
        }
        return result;
    }

    @Override
    public List<OverflowList> getList(String sTime,String eTime) throws ParseException {
        Date sDate = DateUtil.StringToDate(sTime, "yyyy-MM-dd");
        Date eDate = DateUtil.StringToDate(eTime, "yyyy-MM-dd");

        List<OverflowList> overflowLists = overflowDao.getList(sDate,eDate);
        return overflowLists;

    }

    @Override
    public List<OverflowListGoods> getGoodsList(Integer overflowListId) {
        List<OverflowListGoods> goodsList  = overflowDao.getGoodsList(overflowListId);
        return goodsList;
    }
}
