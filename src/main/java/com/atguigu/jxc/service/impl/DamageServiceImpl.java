package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.result.vo.damageList.DamageListVo;
import com.atguigu.jxc.result.vo.damageList.DamageRowsDTO;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.util.DateUtil;
import com.atguigu.jxc.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Service
public class DamageServiceImpl implements DamageService {
    @Autowired
    private DamageDao damageDao;

    @Override
    public boolean save(HttpSession session, DamageList damageList, String damageListGoodsStr) {
        User user = (User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        damageList.setTrueName(user.getTrueName());
//        保存damageList到damage_list
        boolean result = damageDao.saveDamageList(damageList);
//       主键回填id
        Integer damageListId = damageList.getDamageListId();
        if (result) {
            DamageListGoods[] goodsArray = JsonUtils.fromToObject(damageListGoodsStr, DamageListGoods[].class);
            List<DamageListGoods> damageListGoods = Arrays.asList(goodsArray);
            for (DamageListGoods damageListGood : damageListGoods) {
                damageListGood.setDamageListId(damageListId);
                damageDao.saveDamageListGoods(damageListGood);
            }
        }
        return result;
    }

    @Override
    public DamageListVo getDamageList(Integer damageListId) {
        List<DamageRowsDTO> damageRowsDTO = damageDao.getDamageList(damageListId);
        DamageListVo damageListVo = new DamageListVo();
        damageListVo.setRows(damageRowsDTO);
        return damageListVo;
    }

    @Override
    public List<DamageList> list(String sTime, String eTime) throws ParseException {
        Date sDate = DateUtil.StringToDate(sTime, "yyyy-MM-dd");
        Date eDate = DateUtil.StringToDate(eTime, "yyyy-MM-dd");
        List<DamageList> list=   damageDao.getList(sDate,eDate);
        return list;
    }
}
