package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.TSupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.result.vo.supplier.SupplierRowsDTO;
import com.atguigu.jxc.result.vo.supplier.SupplierVo;
import com.atguigu.jxc.service.TSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author xiaojie
 * @description 针对表【t_supplier】的数据库操作Service实现
 * @createDate 2023-09-11 09:31:34
 */
@Service
public class TSupplierServiceImpl implements TSupplierService {


    @Autowired
    private TSupplierDao tSupplierDao;


    @Override
    public SupplierVo getSupplierList(Integer page, Integer rows, String supplierName) {
        SupplierVo supplierVo = new SupplierVo();
//        查询所有供应商
        List<SupplierRowsDTO> rowsDTOList = tSupplierDao.getSupplierDTOList(page - 1, rows, supplierName);
//         查询一共有多少个供货商
        int total = tSupplierDao.getTotalSupplier();

        supplierVo.setTotal(total);
        supplierVo.setRows(rowsDTOList);
        return supplierVo;
    }

    @Override
    public boolean saveOrChangeSupplier(Long supplierId, Supplier supplier) {
//        根据supplierId判断是新增还是修改,新增无id,修改有id
        if (supplierId != null) {
//            说明是修改数据
            boolean result = tSupplierDao.changeSupplier(supplierId, supplier);
            return result;
        }
//        说明是新增数据
        boolean result = tSupplierDao.saveSupplier(supplier);
        return result;
    }

    @Override
    public boolean deleteSupplier(String ids) {
//        取出前端传递的id,分割数据
        String[] split = ids.split(",");
        List<String> list = Arrays.asList(split);
//        批量删除
        boolean result = tSupplierDao.deleteSupplier(list);
        return result;
    }
}




