package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.result.vo.supplier.SupplierVo;

import java.util.List;

/**
* @author xiaojie
* @description 针对表【t_supplier】的数据库操作Service
* @createDate 2023-09-11 09:31:34
*/
public interface TSupplierService {

    /**
     *
     * @param page 起始页
     * @param rows 每页记录数
     * @param supplierName 查询关键字
     * @return
     */
    SupplierVo getSupplierList(Integer page, Integer rows, String supplierName);


    /**
     * 新增或修改供货商信息
     * @param supplierId
     * @param supplier
     * @return
     */
    boolean saveOrChangeSupplier(Long supplierId, Supplier supplier);

    /**
     * 批量删除供应商
     * @param ids
     * @return
     */
    boolean deleteSupplier(String ids);
}
