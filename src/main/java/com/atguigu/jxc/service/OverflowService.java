package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.List;

public interface OverflowService {
//    新增报溢单
    boolean save(HttpSession httpSession, OverflowList overflowList, String overflowListGoodsStr);

//    报溢单查询
    List<OverflowList> getList(String sTime,String eTime) throws ParseException;

//    报溢单商品查询
    List<OverflowListGoods> getGoodsList(Integer overflowListId);
}
