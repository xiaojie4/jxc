package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.result.vo.damageList.DamageListVo;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.List;

public interface DamageService {

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    boolean save(HttpSession session, DamageList damageList, String damageListGoodsStr);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    DamageListVo getDamageList(Integer damageListId);

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> list(String sTime, String eTime) throws ParseException;
}
