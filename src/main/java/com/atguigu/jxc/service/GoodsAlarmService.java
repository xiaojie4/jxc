package com.atguigu.jxc.service;

import com.atguigu.jxc.result.vo.goods.GoodsRowsDTO;

import java.util.List;

public interface GoodsAlarmService {
    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     * @return
     */
    List<GoodsRowsDTO> listAlarm();

}
