package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.result.vo.customer.CustomerVo;

public interface CustomerService {

    /**
     * 分页查询客户列表
     * @param page
     * @param rows
     * @param customerName 模糊查询关键字
     * @return
     */
    CustomerVo getCustomerList(Integer page, Integer rows, String customerName);


    /**
     * 添加或修改客户
     * @param customerId
     * @param customer
     * @return
     */
    boolean saveOrChangeCustomer(Long customerId, Customer customer);

    /**
     * 批量删除客户
     * @param ids
     * @return
     */
    boolean batchDelete(String ids);
}
