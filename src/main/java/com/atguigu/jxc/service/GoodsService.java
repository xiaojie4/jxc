package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.result.vo.goods.GoodsVo;
import com.atguigu.jxc.result.vo.stock.StockVo;
import com.atguigu.jxc.result.vo.unit.UnitVo;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    /**
     * 查询库存
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    StockVo listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 查询商品所有单位
     * @return
     */
    UnitVo getUnitList();


    /**
     * 查询所有商品信息
     * @param page
     * @param rows
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    GoodsVo getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    /**
     * 新增商品分类
     * @param goodsTypeName 商品类型名称
     * @param pId 分类id
     * @return
     */
    boolean addGoodType(String goodsTypeName, Integer pId);

    /**
     * 删除商品分类
     * @param goodsTypeId 分类id
     * @return
     */
    boolean deleteGoodType(Integer goodsTypeId);


    /**
     * 添加或修改商品
     * @param goodsId
     * @param goods
     * @return
     */
    boolean saveOrChangeGoods(Long goodsId, Goods goods);

    /**
     * 删除商品
     * @param goodsId
     * @return
     */
    boolean deleteGoods(Integer goodsId);

    /**
     * 分页查询无库存商品信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    GoodsVo getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);


    /**
     * 分页查询有库存商品信息
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    GoodsVo getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    /**
     *  添加商品期初库存或者修改成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    boolean addOrChangeStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    /**
     * 删除库存
     * @param goodsId
     * @return
     */
    boolean deleteStock(Integer goodsId);
}
