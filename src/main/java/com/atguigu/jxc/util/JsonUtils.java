package com.atguigu.jxc.util;

import com.google.gson.Gson;

public class JsonUtils {
    
    private static Gson gson = new Gson();
    
    /**
     * 将JSON字符串转换为指定类型的对象
     * @param json JSON字符串
     * @param clazz 目标对象的Class
     * @return 转换后的对象
     */
    public static <T> T fromToObject(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }
    
    /**
     * 将对象转换为JSON字符串
     * @param obj 要转换的对象
     * @return 转换后的JSON字符串
     */
    public static String toStr(Object obj) {
        return gson.toJson(obj);
    }
}