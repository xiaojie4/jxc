package com.atguigu.jxc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseVo<T> {
    private int code;
    private String mag;
    private T info;

    public static <T> ResponseVo<T> build(int code, String msg, T info) {
        return new ResponseVo<>(code, msg, info);
    }

}
