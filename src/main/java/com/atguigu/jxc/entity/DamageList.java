package com.atguigu.jxc.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 报损
 */
@Data
@Getter
@Setter
public class DamageList {

  private Integer damageListId;
  private String damageNumber;
  private String damageDate;
  private String remarks;
  private Integer userId;

  private String trueName;

}
