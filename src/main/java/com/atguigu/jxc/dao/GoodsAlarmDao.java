package com.atguigu.jxc.dao;


import com.atguigu.jxc.result.vo.goods.GoodsRowsDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsAlarmDao {
    List<GoodsRowsDTO> listAlarm();
}
