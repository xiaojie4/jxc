package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.result.vo.customer.CustomerRowsDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerDao {
    List<CustomerRowsDTO> getCustomerList(@Param("page") int page, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Integer getTotalCustomer();

    boolean updateCustomer(@Param("customerId") Long customerId, @Param("customer") Customer customer);

    boolean save(@Param("customer") Customer customer);

    boolean deleteCustomer(@Param("ids") List<String> ids);
}
