package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OverflowDao {
    boolean saveOverflowList(@Param("overflowList") OverflowList overflowList);

    void saveOverflowListGoods(@Param("listGood") OverflowListGoods listGood);


    List<OverflowList> getList(@Param("sTime") Date sTime, @Param("eTime") Date eTime);

    List<OverflowListGoods> getGoodsList(@Param("overflowListId") Integer overflowListId);
}
