package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.result.vo.goods.GoodsRowsDTO;
import com.atguigu.jxc.result.vo.stock.StockRowsDTO;
import com.atguigu.jxc.result.vo.unit.UnitRowsDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();


    List<StockRowsDTO> listInventory(@Param("page") Integer page, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    int getTotalGoods();

    // 查询商品类型
    String getGoodTypeName(@Param("goodsTypeId") Integer goodsTypeId);

    //查询商品销售数量
    Integer getGoodSaleTotal(@Param("goodsId") Integer goodsId);

    //查询商品所有单位
   List <UnitRowsDTO> getUnitList();

   List <GoodsRowsDTO> getGoodsList(@Param("page") Integer page, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer totalGoods(@Param("goodsTypeId") Integer goodsTypeId);

    boolean addGoodType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId,@Param("goodsTypeStatus")Integer goodsTypeStatus);

    List<GoodsType> getGoodTypeByPid(@Param("pId") Integer pId);

    boolean updateGoodsTypeStatus(@Param("goodsTypeId") Integer goodsTypeId, @Param("goodsTypeStatus") int goodsTypeStatus);

    boolean deleteGoodType(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType getGoodTypeById(@Param("goodsTypeId") Integer goodsTypeId);

    boolean updateGoods(@Param("goodsId") Long goodsId, @Param("goods") Goods goods);

    boolean saveGoods(@Param("goods") Goods goods);

    Goods getGoodsByGoodId(@Param("goodsId") Integer goodsId);

    boolean deleteGoods(@Param("goodsId") Integer goodsId);

    List<GoodsRowsDTO> getNoInventoryQuantity(@Param("page") Integer page, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Integer totalNoInventoryQuantity();

    List<GoodsRowsDTO> getHasInventoryQuantity(@Param("page") int page, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Integer totalHasInventoryQuantity();

    boolean addOrChangeStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    boolean deleteStock(@Param("goodsId") Integer goodsId);

    List<GoodsRowsDTO> getGoodsALList(@Param("page") int page, @Param("rows") Integer rows, @Param("goodsName") String goodsName);

    Integer totalALLGoods();
}
