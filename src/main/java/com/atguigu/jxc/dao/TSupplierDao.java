package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.result.vo.supplier.SupplierRowsDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
* @author xiaojie
* @description 针对表【t_supplier】的数据库操作Mapper
* @createDate 2023-09-11 09:31:34
* @Entity com.atguigu.jxc.entity.TSupplier
*/
@Repository
public interface TSupplierDao {


    List<SupplierRowsDTO> getSupplierDTOList(@Param("page") Integer page, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    int getTotalSupplier();


    boolean changeSupplier(@Param("supplierId") Long supplierId, @Param("supplier") Supplier supplier);

    boolean saveSupplier(@Param("supplier") Supplier supplier);

    boolean deleteSupplier(@Param("ids") List<String> ids);
}




