package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.result.vo.damageList.DamageRowsDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface DamageDao {
    boolean saveDamageList( @Param("damageList") DamageList damageList);

    boolean saveDamageListGoods(@Param("damageListGoods") DamageListGoods damageListGoods);

    List<DamageRowsDTO> getDamageList(@Param("damageListId") Integer damageListId);

    List<DamageList> getList(@Param("sDate") Date sDate, @Param("eDate") Date eDate);
}
