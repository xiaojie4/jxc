package com.atguigu.jxc.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;
import java.util.Arrays;

@Slf4j
@Component
@Aspect
public class LogAspect {

    @Before("@annotation(com.atguigu.jxc.anno.Log)")
    public void beforeAdvice(JoinPoint joinPoint) {
        String name = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        log.info(name + "方法被调用");
        log.info("请求参数"+Arrays.toString(args));
    }


    @AfterReturning(pointcut = "@annotation(com.atguigu.jxc.anno.Log)", returning = "result")
    public void logMethodExit(JoinPoint joinPoint, Object result) {
        String methodName = joinPoint.getSignature().getName();
        log.info("方法"+methodName+"执行完成,出参是:"+result);
    }

    @AfterThrowing(pointcut = "@annotation(com.atguigu.jxc.anno.Log)",throwing = "exception")
    public void logMethodException(JoinPoint joinPoint, Throwable exception) {
        String methodName = joinPoint.getSignature().getName();

        // 记录方法异常
        log.error(methodName +"执行期间出现了异常:" +  ", 异常信息如下: " + exception.getMessage());
    }

}
