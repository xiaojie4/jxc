package com.atguigu.jxc;

import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.util.JsonUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class JsonUtilTest {

    @Autowired
    private DataSource dataSource;



    @Test
    public void test01() {
        String str = "[{\"goodsId\":1,\"goodsTypeId\":10,\"goodsCode\":\"0001\",\"goodsName\":\"陶华碧老干妈香辣脆油辣椒\",\"goodsModel\":\"红色装\",\"goodsUnit\":\"瓶\",\"lastPurchasingPrice\":8.5,\"price\":\"8.5\",\"goodsNum\":\"2\",\"total\":17},{\"goodsId\":2,\"goodsTypeId\":16,\"goodsCode\":\"0002\",\"goodsName\":\"华为荣耀Note8\",\"goodsModel\":\"Note8\",\"goodsUnit\":\"台\",\"lastPurchasingPrice\":2220,\"price\":\"2220\",\"goodsNum\":\"1\",\"total\":2220}]";
        DamageListGoods[] goodsArray = JsonUtils.fromToObject(str, DamageListGoods[].class);

        List<DamageListGoods> damageListGoods = Arrays.asList(goodsArray);
    }

    @Test
    public void test03() throws SQLException {
        Connection connection = dataSource.getConnection();
        System.out.println("connection = " + connection);

    }
}
